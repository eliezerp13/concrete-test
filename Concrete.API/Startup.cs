﻿using Concrete.CrossCutting.DependencyInjections;
using Concrete.CrossCutting.Documentation;
using Concrete.CrossCutting.Mappings;
using Concrete.CrossCutting.Options;
using Concrete.CrossCutting.Security;
using Concrete.Domain._base;
using Concrete.Domain.Dtos.Responses;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Concrete.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            CrossCutting.Options.Options.Configure(services, Configuration);
            DependencyInjection.Configure(services, Configuration);
            ServicesDependencyInjection.Configure(services);
            Mapping.Configure(services);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1); 
            JWT.Configure(services, Configuration);
            SwaggerDocumentation.Configure(services, Configuration);
          
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWhen(context => !context.Request.Path.Value.StartsWith("/api"), builder =>
            {
                builder.UseStatusCodePagesWithReExecute("/Error/{0}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API 1.0");
                c.DocExpansion(DocExpansion.None);
            });

            // Redireciona o Link para o Swagger, quando acessar a rota principal
            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseHttpsRedirection();
            app.UseMvc();

            app.Use(async (context, next) =>
            {
                await next.Invoke();

                if (context.Response.StatusCode == StatusCodes.Status401Unauthorized)
                {
                    await context.Response.WriteAsync(
                        JsonConvert.SerializeObject(
                            new ErroResponse
                            {
                                StatusCode = 401,
                                Mensagem = Resources.InvalidToken
                            }
                        )
                    );
                }
            });
        }
    }
}
