﻿using Concrete.Domain.Dtos.Responses;
using Concrete.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Concrete.API.Controllers
{
    [Authorize("Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IUsuarioService _service;
        public ProfileController(IUsuarioService service)
        {
            _service = service;
        }
        /// <summary>
        /// Ver dados do usuário (Este método exige autenticação)
        /// </summary>
        /// <param name="id">id*</param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UsuarioDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErroResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponse))]
        public async Task<ActionResult> Profile([FromQuery] Guid id)
        {
            var response = await _service.GetProfile(id);
            return StatusCode(response.Status(), response.Object());
        }
    }
}
