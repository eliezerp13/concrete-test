﻿using Concrete.Domain.Dtos.Requests;
using Concrete.Domain.Dtos.Responses;
using Concrete.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Concrete.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUsuarioService _service;

        public LoginController(IUsuarioService service)
        {
            _service = service;
        }

        /// <summary>
        /// Logar-se
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UsuarioDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponse))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(ErroResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponse))]
        public async Task<ActionResult> Login([FromBody] LoginDto login)
        {
            var response =  await _service.Login(login);
            return StatusCode(response.Status(), response.Object());
        }


    }
}
