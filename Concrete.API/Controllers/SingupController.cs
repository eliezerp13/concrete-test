﻿using Concrete.Domain.Dtos.Requests;
using Concrete.Domain.Dtos.Responses;
using Concrete.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Concrete.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SingupController : ControllerBase
    {
        private readonly IUsuarioService _service;
        public SingupController(IUsuarioService service)
        {
            _service = service;
        }

        /// <summary>
        /// Cadastro
        /// </summary>
        /// <param name="singup"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(UsuarioDto))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ErroResponse))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErroResponse))]
        public async Task<ActionResult> Singup([FromBody] SingupDto singup)
        {
            var response = await _service.Singup(singup);
            return StatusCode(response.Status(), response.Object());
        }
    }
}
