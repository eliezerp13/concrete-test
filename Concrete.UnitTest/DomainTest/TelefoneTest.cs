﻿using Concrete.UnitTest._builders;
using Xunit;

namespace Concrete.UnitTest.DomainTest
{
    public class TelefoneTest
    {
        [Fact(DisplayName = "Deve criar telefone")]
        public void Create()
        {
            var telefone = TelefoneBuilder.New().Build();
            telefone.Validate();
            Assert.True(telefone.Valid);
        }

        [Theory(DisplayName = "Não deve criar telefone com DDD inválido")]
        [InlineData(null)]
        [InlineData("1")]
        [InlineData("100")]
        public void InvalidDDD(string ddd)
        {
            var telefone = TelefoneBuilder.New().WithDdd(ddd).Build();
            telefone.Validate();
            Assert.False(telefone.Valid);
        }

        [Theory(DisplayName = "Não deve criar telefone com numero inválido")]
        [InlineData(null)]
        [InlineData("9999999")]
        [InlineData("")]
        public void InvalidEmail(string numero)
        {
            var telefone = TelefoneBuilder.New().WithNumero(numero).Build();
            telefone.Validate();
            Assert.False(telefone.Valid);
        }
    }
}
