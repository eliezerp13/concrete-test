﻿using Concrete.UnitTest._builders;
using Xunit;

namespace Concrete.UnitTest.DomainTest
{
    public class UsuarioTest
    {
        [Fact(DisplayName = "Deve criar usuário")]
        public void Create()
        {
            var usuario = UsuarioBuilder.New().Build();
            usuario.Validate();
            Assert.True(usuario.Valid);
        }

        [Theory(DisplayName = "Não deve criar usuário com nome inválido")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("Aa")]
        public void InvalidNome(string nome)
        {
            var usuario = UsuarioBuilder.New().WithNome(nome).Build();
            usuario.Validate();
            Assert.False(usuario.Valid);
        }

        [Theory(DisplayName = "Não deve criar usuário com email inválido")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("testando validacao de email")]
        public void InvalidEmail(string email)
        {
            var usuario = UsuarioBuilder.New().WithEmail(email).Build();
            usuario.Validate();
            Assert.False(usuario.Valid);
        }

        [Theory(DisplayName = "Não deve criar usuário com senha inválida")]
        [InlineData("")]
        [InlineData(null)]
        [InlineData("1234")]
        public void InvalidSenha(string senha)
        {
            var usuario = UsuarioBuilder.New().WithSenha(senha).Build();
            usuario.Validate();
            Assert.False(usuario.Valid);
        }
    }
}
