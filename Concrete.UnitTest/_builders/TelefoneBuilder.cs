﻿using Bogus;
using Concrete.Domain.Entities;
using System;

namespace Concrete.UnitTest._builders
{
    public class TelefoneBuilder
    {
        public string Ddd;
        public string Numero;
        public Guid IdUsuario;

        public static TelefoneBuilder New()
        {
            var _faker = new Faker();
            return new TelefoneBuilder
            {
                Ddd = _faker.Random.Int(10, 99).ToString(),
                Numero = _faker.Random.Long(10000000, 999999999).ToString(),
                IdUsuario = Guid.NewGuid()
            };
        }

        public TelefoneBuilder WithDdd(string ddd)
        {
            Ddd = ddd;
            return this;
        }

        public TelefoneBuilder WithNumero(string numero)
        {
            Numero = numero;
            return this;
            
        }

        public TelefoneBuilder WithUsuarioId(Guid idUsuario)
        {
            IdUsuario = idUsuario;
            return this;
        }


        public Telefone Build()
        {
            return new Telefone(Numero, Ddd, IdUsuario);
        }
    }
}
