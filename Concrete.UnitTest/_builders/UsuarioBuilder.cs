﻿using Bogus;
using Concrete.Domain.Entities;

namespace Concrete.UnitTest._builders
{
    public class UsuarioBuilder
    {
        private string Nome;
        private string Email;
        private string Senha;

        public static UsuarioBuilder New()
        {
            var _faker = new Faker();
            return new UsuarioBuilder
            {
                Nome = _faker.Person.FullName,
                Email = _faker.Person.Email,
                Senha = _faker.Internet.Password()
            };

        }

        public UsuarioBuilder WithNome(string nome)
        {
            Nome = nome;
            return this;
        }

        public UsuarioBuilder WithEmail(string email)
        {
            Email = email;
            return this;
        }

        public UsuarioBuilder WithSenha(string senha)
        {
            Senha = senha;
            return this;
        }

        public Usuario Build()
        {
            return new Usuario(Nome, Email, Senha);
        }
    }
}
