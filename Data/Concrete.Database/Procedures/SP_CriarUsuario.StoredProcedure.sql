﻿USE CONCRETE
GO;

CREATE PROCEDURE SP_CriarUsuario
	@ID uniqueidentifier,
   @NOME as varchar(100),
   @EMAIL as varchar(100),
   @SENHA as varchar(500),
   @DATACRIACAO as datetime,
   @DATAATUALIZACAO as datetime,
   @ULTIMOLOGIN as datetime,
   @TOKEN as uniqueidentifier
as
INSERT INTO [dbo].[USUARIOS]
           ([ID]
           ,[NOME]
           ,[EMAIL]
           ,[SENHA]
           ,[DATACRIACAO]
           ,[DATAATUALIZACAO]
           ,[ULTIMOLOGIN]
           ,[TOKEN])
     VALUES
           	(@ID,
			@NOME,
			@EMAIL,
			@SENHA,
			@DATACRIACAO,
			@DATAATUALIZACAO,
			@ULTIMOLOGIN,
			@TOKEN)
