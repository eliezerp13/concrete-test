﻿using Concrete.Data.Mapping;
using Concrete.Domain.Entities;
using Flunt.Notifications;
using Microsoft.EntityFrameworkCore;

namespace Concrete.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Usuario> Usuarios{ get; set; }
        public DbSet<Telefone> Telefones { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Ignore<Notification>();
            modelBuilder.Entity<Usuario>(new UsuarioMap().Configure);
            modelBuilder.Entity<Telefone>(new TelefoneMap().Configure);

        }
    }
}
