﻿using Concrete.Data._base;
using Concrete.Domain.Entities;
using Concrete.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Concrete.Data.Repositories
{
    public class TelefoneRepository : Repository<Telefone>, ITelefoneRepository
    {
        private readonly DbSet<Telefone> _dataset;

        public TelefoneRepository(AppDbContext context) : base(context)
        {
            _dataset = context.Set<Telefone>();
        }
    }
}
