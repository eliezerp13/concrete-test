﻿using Concrete.Data._base;
using Concrete.Domain.Entities;
using Concrete.Domain.Interfaces.Repositories;
using Concrete.Domain.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Concrete.Data.Repositories
{
    public class UsuarioRepository : Repository<Usuario>, IUsuarioRepository
    {
        private readonly DbSet<Usuario> _dataset;
        private readonly string _connectionString;

        public UsuarioRepository(AppDbContext context, IOptions<ConfigurationsOptions> options) : base(context)
        {
            _dataset = context.Set<Usuario>();
            _connectionString = options.Value.ConnectionString;
        }

        public async Task<Usuario> SelectByEmail(string email)
        {
            var result = await _dataset
                .Include("Telefones")
                .FirstOrDefaultAsync(x => x.Email.Equals(email));
            return result;
        }    

        public async Task<Usuario> SelectAll(Guid Id)
        {
            var result = await _dataset
                .Include("Telefones")
                .FirstOrDefaultAsync(x => x.Id == Id);

            return result;
        }


        public void InsertAdoNet(Usuario usuario)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {   //@ID, @NOME,@EMAIL, @SENHA, @DATACRIACAO, @DATAATUALIZACAO, @ULTIMOLOGIN, @TOKEN

                    con.Open();
                    var cmd = new SqlCommand("SP_CriarUsuario", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", usuario.Id);
                    cmd.Parameters.AddWithValue("@NOME", usuario.Nome);
                    cmd.Parameters.AddWithValue("@EMAIL", usuario.Email);
                    cmd.Parameters.AddWithValue("@SENHA", usuario.Senha);
                    cmd.Parameters.AddWithValue("@DATACRIACAO", usuario.DataCriacao);
                    cmd.Parameters.AddWithValue("@DATAATUALIZACAO", usuario.DataAtualizacao);
                    cmd.Parameters.AddWithValue("@ULTIMOLOGIN", usuario.UltimoLogin);
                    cmd.Parameters.AddWithValue("@TOKEN", usuario.Token);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            

        }
    }
}
