﻿using AutoMapper;
using Concrete.Domain._base;
using Concrete.Domain.Dtos.Requests;
using Concrete.Domain.Dtos.Responses;
using Concrete.Domain.Entities;
using Concrete.Domain.Interfaces.Repositories;
using Concrete.Domain.Interfaces.Services;
using Concrete.Services._base;
using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Concrete.Services.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _repository;
        private readonly ITelefoneRepository _telefoneRepository;
        private readonly IMapper _mapper;
        private IToken _token;

        public UsuarioService(IUsuarioRepository repository,
                              ITelefoneRepository telefoneRepository,
                              IMapper mapper,
                              IToken token
                )
        {
            _repository = repository;
            _telefoneRepository = telefoneRepository;
            _mapper = mapper;
            _token = token;
        }

        public async Task<IGenericResponse> Login(LoginDto login)
        {
            try
            {
                
                var usuario = await _repository.SelectByEmail(login.Email);
                if (usuario == null)
                    return GenericResponse.CreateErrorResponse(HttpStatusCode.BadRequest, Resources.InvalidUser);

                if (usuario.Senha != Functions.Md5Encrypt(login.Senha))
                    return GenericResponse.CreateErrorResponse(HttpStatusCode.Unauthorized, Resources.InvalidUser);

                usuario.AtualizarUltimoLogin();
                await _repository.Update(usuario);

                var usuarioDto = _mapper.Map<UsuarioDto>(usuario);
                usuarioDto.Token = _token.CreateToken(usuario.Token, usuario.Id);

                return GenericResponse.CreateSuccessResponse(HttpStatusCode.OK, usuarioDto);

            }
            catch (Exception e)
            {
                return GenericResponse.CreateInternalErrorResponse();
            }
        }

        public async Task<IGenericResponse> GetProfile(Guid id)
        {
            try
            {
                if(id == Guid.Empty)
                    return GenericResponse.CreateErrorResponse(HttpStatusCode.BadRequest, Resources.RequiredUser);

                var usuario = await _repository.SelectAll(id);
                if (usuario == null)
                    return GenericResponse.CreateErrorResponse(HttpStatusCode.NotFound, Resources.InvalidUser);

                return GenericResponse.CreateSuccessResponse(HttpStatusCode.OK, _mapper.Map<UsuarioDto>(usuario));

            }
            catch (Exception e)
            {
                return GenericResponse.CreateInternalErrorResponse();
            }
        }

        public async Task<IGenericResponse> Singup(SingupDto singup)
        {
            try
            {
                var usuario = new Usuario(singup.Nome, singup.Email,singup.Senha);
                usuario.Validate();

                var notifications = new List<Notification>();
                notifications.AddRange(usuario.Notifications);

                var telefones = new List<Telefone>();

                foreach(var telefoneSingup in singup.Telefones){
                    var telefone = new Telefone(telefoneSingup.Numero, telefoneSingup.Ddd, usuario.Id);
                    telefone.Validate();
                    telefones.Add(telefone);
                    notifications.AddRange(telefone.Notifications);
                }

                if (notifications.Count > 0)
                    return GenericResponse.CreateResponseWithNotifications(notifications.Distinct().ToList());

                var existentEmail = await _repository.SelectByEmail(usuario.Email);
                if (existentEmail != null)
                    return GenericResponse.CreateErrorResponse(HttpStatusCode.BadRequest, Resources.EmailAlreadyExists);

                usuario.GerarCriptografia();

                _repository.InsertAdoNet(usuario);
                telefones.ForEach(async x => await _telefoneRepository.Insert(x));

                usuario.Telefones = telefones;

                var usuarioDto = _mapper.Map<UsuarioDto>(usuario);
                usuarioDto.Token = _token.CreateToken(usuario.Token, usuario.Id);
                return GenericResponse.CreateSuccessResponse(HttpStatusCode.Created, usuarioDto);

            }
            catch (Exception e)
            {
                return GenericResponse.CreateInternalErrorResponse();
            }
        }
    }
}
