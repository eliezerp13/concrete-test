﻿using Concrete.Domain._base;
using Concrete.Domain.Dtos.Responses;
using Flunt.Notifications;
using System.Collections.Generic;
using System.Net;

namespace Concrete.Services._base
{
    public class GenericResponse : IGenericResponse
    {
        public int StatusCode { get; private set; }
        public object Data { get; private set; }

        public static GenericResponse CreateResponseWithNotifications(IReadOnlyCollection<Notification> notifications)
        {
            return new GenericResponse
            {
                StatusCode = (int)HttpStatusCode.BadRequest,
                Data = new ErroResponse
                {
                    StatusCode = (int)HttpStatusCode.BadRequest,
                    Mensagem = Functions.NotificationsToString(notifications)
                }
            };
        }

        public int Status()
        {
            return StatusCode;
        }

        public object Object()
        {
            return Data;
        }

        public static GenericResponse CreateErrorResponse(HttpStatusCode statusCode, string message)
        {
            return new GenericResponse
            {
                StatusCode = (int)statusCode,
                Data = new ErroResponse()
                {
                    StatusCode = (int)statusCode,
                    Mensagem = message
                }
            };
        }

        public static GenericResponse CreateInternalErrorResponse()
        {
            return new GenericResponse
            {
                StatusCode = (int)HttpStatusCode.InternalServerError,
                Data = new ErroResponse()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Mensagem = Resources.InternalServerError
                }
            };
        }

        public static GenericResponse CreateSuccessResponse(HttpStatusCode statusCode, object data)
        {
            return new GenericResponse
            {
                StatusCode = (int)statusCode,
                Data = data
            };
        }

    }
}
