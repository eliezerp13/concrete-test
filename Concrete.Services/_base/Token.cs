﻿using Concrete.Domain._base;
using Concrete.Domain.Options;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;

namespace Concrete.Services._base
{
    public class Token : IToken
    {
        private readonly SigningConfigurations _signingConfigurations;
        private readonly ConfigurationsOptions _options;
        public Token(SigningConfigurations signingConfigurations, IOptions<ConfigurationsOptions> options)
        {
            _signingConfigurations = signingConfigurations;
            _options = options.Value;
        }

        public string CreateToken(Guid userToken, Guid idUsuario)
        {
            var identity = new ClaimsIdentity(
                       new GenericIdentity(idUsuario.ToString()),
                       new[]
                       {
                            new Claim(JwtRegisteredClaimNames.Jti, userToken.ToString()),
                            new Claim(JwtRegisteredClaimNames.UniqueName, idUsuario.ToString()),
                       }
                   );
            DateTime createDate = DateTime.Now;
            DateTime expirationDate = createDate + TimeSpan.FromMinutes(30);

            var handler = new JwtSecurityTokenHandler();
            return CreateToken(identity, createDate, expirationDate, handler);
        }

        private string CreateToken(ClaimsIdentity identity, DateTime createDate, DateTime expirationDate, JwtSecurityTokenHandler handler)
        {
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = _options.JwtConfig.Issuer,
                Audience = _options.JwtConfig.Audience,
                SigningCredentials =_signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = createDate,
                Expires = expirationDate,
            });

            var token = handler.WriteToken(securityToken);
            return token;
        }
    }

}
