﻿using Concrete.Data;
using Concrete.Data._base;
using Concrete.Data.Repositories;
using Concrete.Domain._base;
using Concrete.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Concrete.CrossCutting.DependencyInjections
{
    public class RepositoryDependencyInjection
    {
        public static void Configure(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            serviceCollection.AddScoped<IUsuarioRepository, UsuarioRepository>();
            serviceCollection.AddScoped<ITelefoneRepository, TelefoneRepository>();
            var connectionString = configuration.GetSection("Configurations")["ConnectionString"];

            serviceCollection.AddDbContext<AppDbContext>(
                options => options.UseSqlServer(connectionString)
            );
        }
    }
}
