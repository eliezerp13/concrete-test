﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Concrete.CrossCutting.DependencyInjections
{
    public class DependencyInjection
    {
        public static void Configure(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            RepositoryDependencyInjection.Configure(serviceCollection, configuration);
            ServicesDependencyInjection.Configure(serviceCollection);
        }
    }
}
