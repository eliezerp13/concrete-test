﻿using Concrete.Domain._base;
using Concrete.Domain.Interfaces.Services;
using Concrete.Services._base;
using Concrete.Services.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Concrete.CrossCutting.DependencyInjections
{
    public class ServicesDependencyInjection
    {
        public static void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IToken, Token>();
            serviceCollection.AddTransient<IUsuarioService, UsuarioService>();
        }
    }
}
