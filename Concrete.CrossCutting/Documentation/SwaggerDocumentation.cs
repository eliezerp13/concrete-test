﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Concrete.CrossCutting.Documentation
{
    public class SwaggerDocumentation
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new Info
                    {
                        Title = "Concrete API",
                        Version = "v1.0",
                        Description = "API teste para Concrete",
                        Contact = new Contact
                        {
                            Name = "Created By Eliezer Cútalo",
                            Url = "www.google.com",
                            Email = "eliezercsj@gmail.com"
                        }
                    });

                //Colocar JWT no Swagger
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "Entre com o Token JWT",
                    Name = "Authorization",
                    Type = "apiKey"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                   { "Bearer", Enumerable.Empty<string>() },
                });
            });
        }
    }
}
