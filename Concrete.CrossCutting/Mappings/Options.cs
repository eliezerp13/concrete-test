﻿using Concrete.Domain.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Concrete.CrossCutting.Options
{
    public class Options
    {

        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddOptions();
            services.Configure<ConfigurationsOptions>(configuration.GetSection("Configurations"));
            

        }
    }
}
