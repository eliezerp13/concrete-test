﻿using AutoMapper;
using Concrete.Domain.Dtos.Responses;
using Concrete.Domain.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Concrete.CrossCutting.Mappings
{
    public class Mapping : Profile
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddSingleton(
                new MapperConfiguration(cfg => {

                    TelefoneDtoMap(cfg);
                    UserDtoMap(cfg);

                }).CreateMapper()
            );
        }

        private static void TelefoneDtoMap(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Telefone, TelefoneDto>()
                .ForMember(dto => dto.Id, m => m.MapFrom(entity => entity.Id))
                .ForMember(dto => dto.Numero, m => m.MapFrom(entity => entity.Numero))
                .ForMember(dto => dto.Ddd, m => m.MapFrom(entity => entity.Ddd));
        }

        private static void UserDtoMap(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Usuario, UsuarioDto>()
                .ForMember(dto => dto.Id, m => m.MapFrom(entity => entity.Id))
                .ForMember(dto => dto.Email, m => m.MapFrom(entity => entity.Email))
                .ForMember(dto => dto.Telefones, m => m.MapFrom(entity => entity.Telefones != null ? entity.Telefones : new List<Telefone>()))
                .ForMember(dto => dto.DataCriacao, m => m.MapFrom(entity => entity.DataCriacao))
                .ForMember(dto => dto.DataAtualizacao, m => m.MapFrom(entity => entity.DataAtualizacao))
                .ForMember(dto => dto.UltimoLogin, m => m.MapFrom(entity => entity.UltimoLogin));
            ;
        }
    }
}
