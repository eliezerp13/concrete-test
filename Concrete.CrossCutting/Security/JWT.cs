﻿using Concrete.Domain.Options;
using Concrete.Services._base;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace Concrete.CrossCutting.Security
{
    public class JWT
    {
        public static void Configure(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var builderService = serviceCollection.BuildServiceProvider();
            var options = builderService.GetService<IOptions<ConfigurationsOptions>>().Value;

            var signingConfigurations = new SigningConfigurations();
            serviceCollection.AddSingleton(signingConfigurations);
            serviceCollection.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = signingConfigurations.Key;
                paramsValidation.ValidAudience = options.JwtConfig.Audience;

                paramsValidation.ValidIssuer = options.JwtConfig.Issuer;
                paramsValidation.ValidateIssuerSigningKey = true;

                // Verifica se um token recebido ainda é válido
                paramsValidation.ValidateLifetime = true;
                paramsValidation.ClockSkew = TimeSpan.Zero;
            });

            serviceCollection.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());
            });
        }
    }
}
