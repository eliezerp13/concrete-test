﻿using Concrete.Domain._base;
using Concrete.Domain.Dtos.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Concrete.Domain.Interfaces.Services
{
    public interface IUsuarioService
    {
        Task<IGenericResponse> Login(LoginDto login);
        Task<IGenericResponse> GetProfile(Guid id);
        Task<IGenericResponse> Singup(SingupDto singup);
    }
}
