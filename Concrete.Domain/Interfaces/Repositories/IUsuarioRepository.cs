﻿using Concrete.Domain._base;
using Concrete.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Concrete.Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Task<Usuario> SelectAll(Guid Id);
        Task<Usuario> SelectByEmail(string email);
        void InsertAdoNet(Usuario usuario);
    }
}
