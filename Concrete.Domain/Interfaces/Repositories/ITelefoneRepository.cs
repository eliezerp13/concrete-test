﻿using Concrete.Domain._base;
using Concrete.Domain.Entities;

namespace Concrete.Domain.Interfaces.Repositories
{
    public interface ITelefoneRepository : IRepository<Telefone>
    {
        
    }
}
