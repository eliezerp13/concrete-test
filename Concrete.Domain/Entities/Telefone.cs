﻿using Concrete.Domain._base;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Concrete.Domain.Entities
{
    public class Telefone : Entity, IValidator
    {
        private Telefone() { }

        public Telefone(string numero, string ddd, Guid idUsuario)
        {
            Numero = numero;
            Ddd = ddd;
            IdUsuario = idUsuario;
        }

        public string Numero { get; private set; }
        public string Ddd { get; private set; }

        [ForeignKey("Usuario")]
        public Guid IdUsuario { get; private set; }

        public virtual Usuario Usuario { get; private set; }

        public void Validate()
        {
            AddNotifications(
                new Contract()
                    .Requires()
                    .HasLen(Ddd, 2, "DDD", Resources.InvalidDDD)
                    .HasMinLen(Numero, 8, "Número", Resources.InvalidNumber)
            );
        }
    }
}
