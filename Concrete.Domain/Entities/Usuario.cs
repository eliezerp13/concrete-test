﻿using Concrete.Domain._base;
using Flunt.Notifications;
using Flunt.Validations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Concrete.Domain.Entities
{
    public class Usuario : Entity, IValidator
    {
        private Usuario(){}

        public Usuario(string nome, string email, string senha)
        {
            Nome = nome;
            Email = email;
            Senha = senha;
            DataCriacao = DateTime.Now;
            DataAtualizacao = DateTime.Now;
            UltimoLogin = DateTime.Now;
            Token = Guid.NewGuid();
        }

        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Senha { get; private set; }
        public DateTime DataCriacao { get; private set; }
        public DateTime DataAtualizacao { get; private set; }
        public DateTime UltimoLogin { get; set; }
        public Guid Token { get; private set; }

        public virtual ICollection<Telefone> Telefones { get; set; }

        public void AtualizarUltimoLogin()=> UltimoLogin = DateTime.Now;
        public void GerarCriptografia() => Senha = Functions.Md5Encrypt(Senha);

        public void Validate()
        {
            AddNotifications(
                new Contract()
                    .Requires()
                    .HasMinLen(Nome, 3, "Nome", Resources.InvalidName)
                    .IsEmail(Email, "Email", Resources.InvalidEmail)
                    .HasMinLen(Senha, 5,"Senha", Resources.InvalidPassword)
            );
        }
    }
}
