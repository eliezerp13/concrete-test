﻿namespace Concrete.Domain.Options
{
    public class JwtConfigOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }

    }
}
