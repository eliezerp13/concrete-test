﻿namespace Concrete.Domain.Options
{
    public class ConfigurationsOptions
    {
        public string ConnectionString { get; set; }
        public JwtConfigOptions JwtConfig { get; set; }
    }
}
