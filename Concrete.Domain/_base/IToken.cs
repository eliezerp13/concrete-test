﻿using System;

namespace Concrete.Domain._base
{
    public interface IToken
    {
        string CreateToken(Guid userToken, Guid idUsuario);
    }
}
