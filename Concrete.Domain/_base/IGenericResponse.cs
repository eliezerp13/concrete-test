﻿using System.Net;

namespace Concrete.Domain._base
{
    public interface IGenericResponse
    {
        int Status();
        object Object();
    }
}
