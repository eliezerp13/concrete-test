﻿using Flunt.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Concrete.Domain._base
{
    public static class Functions
    {
        public static string NotificationsToString(IReadOnlyCollection<Notification> notifications)
        {
            var texto ="";
            foreach(var notification in notifications)
                texto += $"{notification.Message}, ";

            texto = texto.Substring(0, texto.Count() - 2);
            return texto;
        }

        public static string Md5Encrypt(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
