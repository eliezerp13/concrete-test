﻿using Flunt.Validations;

namespace Concrete.Domain._base
{
    public interface IValidator : IValidatable
    {
    }
}
