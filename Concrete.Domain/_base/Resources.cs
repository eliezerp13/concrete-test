﻿namespace Concrete.Domain._base
{
    public static class Resources
    {
        public static string InternalServerError = "Ocorreu um erro inesperado! Tente novamente mais tarde";
        public static string InvalidDDD = "DDD precisa conter 2 dígitos!";
        public static string InvalidNumber = "Número de telefone precisa conter no mínimo 8 dígitos!";
        public static string InvalidName = "Nome inválido!";
        public static string InvalidEmail = "Email inválido!";
        public static string InvalidPassword = "Senha precisa conter no mínimo 5 caracteres!";
        public static string EmailAlreadyExists = "Este e-mail já está sendo utilizado";
        public static string InvalidUser = "Usuário e/ou e-mail inválidos";
        public static string RequiredUser = "Id do usuário é obrigatório";
        public static string InvalidRoute = "Endpoint inválido";
        public static string InvalidToken = "Token inválido";
    }
}
