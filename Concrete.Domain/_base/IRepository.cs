﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Concrete.Domain._base
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> Insert(T item);
        Task<T> Update(T item);
        Task<bool> Delete(Guid id);
        Task<T> Select(Guid id);
        Task<IEnumerable<T>> Select();
    }
}
