﻿using Flunt.Notifications;
using System;
using System.ComponentModel.DataAnnotations;

namespace Concrete.Domain._base
{
    public abstract class Entity : Notifiable
    {
        [Key]
        public Guid Id { get; private set; }

        protected Entity()
        {
            Id = Guid.NewGuid();
        }
    }
}
