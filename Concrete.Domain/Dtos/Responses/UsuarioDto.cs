﻿using Concrete.Domain.Dtos.Requests;
using System;
using System.Collections.Generic;

namespace Concrete.Domain.Dtos.Responses
{
    public class UsuarioDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public IList<TelefoneDto> Telefones { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public DateTime UltimoLogin { get; set; }
        public string Token { get; set; }
    }
}
