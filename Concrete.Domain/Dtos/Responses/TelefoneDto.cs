﻿using System;

namespace Concrete.Domain.Dtos.Responses
{
    public class TelefoneDto
    {
        public Guid Id { get; set; }
        public string Numero { get; set; }
        public string Ddd { get; set; }
    }
}
