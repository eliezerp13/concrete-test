﻿namespace Concrete.Domain.Dtos.Responses
{
    public class ErroResponse
    {
        public int StatusCode { get; set; }
        public string Mensagem { get; set; }
    }
}
