﻿using System;

namespace Concrete.Domain.Dtos.Requests
{
    public class TelefoneSingupDto
    {
        public string Numero { get; set; }
        public string Ddd { get; set; }

    }
}
