﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Concrete.Domain.Dtos.Requests
{
    public class SingupDto
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public IList<TelefoneSingupDto> Telefones { get; set; } = new List<TelefoneSingupDto>();

    }
}
